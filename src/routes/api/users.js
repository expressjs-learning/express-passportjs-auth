/* 
*@ Seperate router file
*/
// const UserController=require("./../../controllers/userController")
const express=require("express")
const userController = require("./../../controllers/userController")

const router=express.Router()

/* 
*@ METHOD:GET
*@ DESC Renders home page
*@ ACCESS Public
*/
router.get("/",userController.home)

/* 
*@ METHOD:GET
*@ DESC Renders Login page
*@ ACCESS Public
*/
router.get("/login",userController.login)

/* 
*@ METHOD:GET
*@ DESC Renders register page
*@ ACCESS Public
*/
router.get("/register",userController.register)

/* 
*@ METHOD:GET
*@ DESC Renders Progile page
*@ ACCESS Public
*/
router.get("/profile",userController.profile)

/* 
*@ METHOD:POST
*@ DESC create login route
*@ ACCESS Public
*/
router.post("/login",userController.authenticate)

/* 
*@ METHOD:POST
*@ DESC create register route
*@ ACCESS Public
*/
router.post("/create",userController.create)

module.exports=router