// Modules to require
const User=require("../model/User")
const passport=require("passport")
// export objects
module.exports={
    home:(req,res)=>{
        res.render("home")
    },

    login:(req,res)=>{
        res.render("login")
    },

    register:(req,res)=>{
        res.render("register")
    },

    profile:(req,res)=>{
        res.render("profile")
    },

    authenticate:passport.authenticate("local",{
        failureRedirect:"/login",
        failureMessage:"Error logging in",
        successRedirect:"/profile",
        successMessage:"successfully logged in"
    }),

    create:(req,res,next)=>{
        
        let newUser=new User({
            name:req.body.name,
            username:req.body.username,
            password:req.body.password,
            email:req.body.email
        })
        User.register(newUser,req.body.password,(err,user)=>{
            if(user){
                req.flash("success",`User ${user.name} successfully registered`)
                res.redirect("/profile")
                next()
            }else{
                req.flash("error","Registration not successful! try again")
                console.log(err)
                res.locals.redirect="/register"
                next()
            }
        })
    }
}