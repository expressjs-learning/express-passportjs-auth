const mongoose=require("mongoose");
const LocalMongooseStrategy=require("passport-local-mongoose")
const {Schema}=mongoose

const UserSchema=new Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    username:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    data:{
        type:Date,
        default:Date.now
    }
})
UserSchema.plugin(LocalMongooseStrategy,{
    usernameField:"email"
})

module.exports=mongoose.model("User",UserSchema)