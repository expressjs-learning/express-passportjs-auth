/* 
*@This mongoose config file sets up the mongodb database for this express application
*/
const mongoose=require("mongoose")

mongoose.Promise = global.Promise;
// Require MongoURI from the config file
const db=require("./keys").MongoURI

// Connect mongoose to Mongo Atlas
mongoose.connect(db,{useNewUrlParser:true}).then(()=>{
    console.log(`Mongoose successfully connects to the mongoose database`)
}).catch(err=>{
    console.log(`err`)
})