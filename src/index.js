/* 
*@ Require Express for Server
*@ Require Passport for authentication
*@ Require cookieparser to decode and encode cookies
*@ Require expresssession to store data needed in the browser
*@ Require connectFlash ro create flash messages
*/
const express=require("express")
const passport=require("passport")
const cookierParser=require("cookie-parser")
const expressSession=require("express-session")
const connectFlash=require("connect-flash")

// Create express instance
const app=express()


// import Mongoose connection to the entry js file
const Mongooseconnect=require("./config/mongoose.config")


// Set view engine using ejs
app.set("view engine","ejs")

// Require secret keys from the config folder
const secret=require("./config/keys").secret

app.use(express.urlencoded({extended:false}))
app.use(express.json())

app.use(cookierParser(secret))
app.use(expressSession({
    secret:secret,
    cookie:{
        maxAge:4000000
    },
    resave:false,
    saveUninitialized:false
}))

// Initialize Passport
app.use(passport.initialize())
app.use(passport.session())

// Set up passport User
const User=require("./model/User")
passport.use(User.createStrategy())
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())

app.use(connectFlash())

app.use((req,res,next)=>{
    res.locals.flashMessages=req.flash()
    next()
})
app.use((req,res,next)=>{
    console.log(req.url)
    next()
})

// Require and use routes
const routes=require("./routes/api/users")
const { use } = require("./routes/api/users")
app.use(routes)


// listen to the server
const PORT=process.env.PORT||3000
app.listen(PORT,()=>{
    console.log(`Application server running on port ${PORT}`)
})